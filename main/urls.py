from collections import namedtuple
from django.urls import path
from .views import RegisterUserView, index, other_page, BBLoginView, profile, BBLogoutView, ChangeUserInfoView,BBPasswordChangeView
from .views import RegisterUserView, RegisterDoneView, user_activate, DeleteUserView
from .views import by_rubric
from .views import detail


app_name = 'main'

urlpatterns = [
    path('accounts/register/activate/<str:sign>/', user_activate, name='register_activate'),
    path('acconts/register/done/', RegisterDoneView.as_view(), name='register_done'),
    path('accounts/register/', RegisterUserView.as_view(), name='register'),
    path('accounts/logout/', BBLogoutView.as_view(), name='logout'),
    path('accounts/profile/delete/', DeleteUserView.as_view(), name='profile_delete'),
    path('accounts/password/change/', BBPasswordChangeView.as_view(), name='password_change'),
    path('acconts/profile/change', ChangeUserInfoView.as_view(), name='profile_change'),
    path('accounts/profile/', profile, name='profile'),
    path('accounts/login/', BBLoginView.as_view(), name='login'),
    path('<int:rubric_pk>/<int:pk>/', detail, name='detail'),
    path('<str:pk>/', by_rubric, name='by_rubric'),
    path('<str:page>/', other_page, name='other'),
    path('', index, name='index'),
]